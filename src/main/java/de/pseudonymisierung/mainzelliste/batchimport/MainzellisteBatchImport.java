package de.pseudonymisierung.mainzelliste.batchimport;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import de.pseudonymisierung.mainzelliste.client.MainzellisteConnection;
import de.pseudonymisierung.mainzelliste.client.Session;

public class MainzellisteBatchImport {

    public static void main(String args[]) {

        if (args.length < 4) {
            System.out.println(
                    "Expects four parameters: URL of Mainzelliste, API Key, input and output file (CSV files).");
            System.exit(1);
        }

        File inputFile = new File(args[2]);
        if (!inputFile.exists()) {
            System.err.println("Input file " + inputFile + " does not exist!");
        }
        File outputFile = new File(args[3]);
        CSVPrinter csvFilePrinter = null;
        try (FileReader in = new FileReader(inputFile);
                FileWriter out = new FileWriter(outputFile);
                CSVParser csvFileParser = new CSVParser(in, CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader());) {

            ArrayList<String> headerNames = new ArrayList<>(csvFileParser.getHeaderMap().size() + 3);
            csvFileParser.getHeaderMap().forEach((key, index) -> headerNames.add(index, key));
            headerNames.add("idType");
            headerNames.add("idString");
            headerNames.add("tentative");
            csvFilePrinter = new CSVPrinter(out,
                    CSVFormat.EXCEL.withDelimiter(';').withHeader(headerNames.toArray(new String[headerNames.size()])));

            MainzellisteConnection connection = new MainzellisteConnection(args[0], args[1]);
            HttpClient httpClient = HttpClientBuilder.create().build();
            Session session = connection.createSession();
            int counter = 0;
            for (CSVRecord record : csvFileParser.getRecords()) {
                String tokenId = session.getAddPatientToken(null, null);
                HttpPost postAddPatient = new HttpPost(
                        connection.getMainzellisteURI().resolve("patients?tokenId=" + tokenId));
                postAddPatient.setHeader("mainzellisteApiVersion", "2.0");
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                for (Entry<String, String> entry : record.toMap().entrySet()) {
                    params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                // Set sureness=true to prevent 409 errors (creates tentative
                // IDs for potential matches)
                params.add(new BasicNameValuePair("sureness", "true"));
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
                postAddPatient.setEntity(entity);
                HttpResponse response = httpClient.execute(postAddPatient);
                String responseString = EntityUtils.toString(response.getEntity());
                JSONArray responseJSON = new JSONArray(responseString);
                JSONObject newId = responseJSON.getJSONObject(0);
                for (String value : record) {
                	csvFilePrinter.print(value);
                }
                csvFilePrinter.print(newId.getString("idType"));
                csvFilePrinter.print(newId.getString("idString"));
                csvFilePrinter.print(newId.getBoolean("tentative"));
                csvFilePrinter.println();
                counter++;
                System.out.print("\rProcessed " + counter + " records");
            }
            csvFilePrinter.close();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }
}
