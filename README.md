# Mainzelliste.Batchimport

Command line tool to import patients into a Mainzelliste instance from a CSV file.

## Build

In order to build Mainzelliste.Client, you need to add the Samply Maven Repository to your Maven settings. See <https://maven.mitro.dkfz.de/external.html> for instructions on how to do this.

Use maven to build the jar:

``` 
mvn clean package
```

## Invocation

After building, an executable .jar file is located in the output folder (`target`). From the project directory, run the following command to start:

```
java -jar target/mainzelliste-batchimport.jar {URL} {API key} {input file} {output file}
``` 

Arguments:

* `{URL}`: Base url of the mainzelliste instance. E.g. when running in a local development environment, a typical URL is `http://localhost:8080/mainzelliste`
* `{API key}`: The access key as defined in the `servers.{x}` setting in the Mainzelliste configuration file.
* `{input file}`, `{output file}`: File names of input and output file.

## File format

Currently, the input file is expected to be in the format that MS Excel (at least the German version) uses to create CSV files, i.e.:
* Values are separated by semicolons.
* The first line holds the header, i.e. the field names.
* Values can be enclosed by double quotes (`"`). If a double quote is part of a value, it has to be escaped by another double quote, in this case, the whole value has to be enclosed in quotes. For example: `"This is a double quote: """`. 

The field names in the header must match the input fields configured in the Mainzelliste configuration file. They can appear in any order.

In the output file, the following fields are added:

* `idType`: Type (name) of the generated ID.
* `idString`: Value of the generated ID.
* `tentative`: Boolean; if true, the generated ID is tentative, i.e. the record linkage found a possible match.
